# syntax = docker/dockerfile:1.5

#   Copyright 2020 Larry1123
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

FROM golang:1.19-alpine3.17 as gobuild
ENV GO111MODULE=on
ENV CGO_ENABLED=0

ARG CLOUDFLARED_VERSION=2023.8.2

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        git \
        gcc \
        build-base \
    ;

WORKDIR /go/src/github.com/cloudflare/

RUN git clone --depth 1 --branch ${CLOUDFLARED_VERSION} https://github.com/cloudflare/cloudflared.git

WORKDIR /go/src/github.com/cloudflare/cloudflared

RUN --mount=type=cache,target=/root/.cache/go-build make cloudflared

FROM alpine:3.16

WORKDIR /home/cloudflare

ARG UID=1000
ARG GID=1000

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        dumb-init \
        su-exec \
        ca-certificates \
    ;

COPY docker-entrypoint.sh /usr/local/bin/

RUN addgroup -S -g ${GID} cloudflare && adduser -S -G cloudflare -u ${UID} cloudflare

RUN su-exec cloudflare mkdir /home/cloudflare/.cloudflared

COPY --from=gobuild /go/src/github.com/cloudflare/cloudflared/cloudflared /usr/local/bin

ENTRYPOINT ["dumb-init", "--", "docker-entrypoint.sh"]
CMD ["cloudflared"]
