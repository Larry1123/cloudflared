larry1123/cloudflared
===
[![pipeline status](https://gitlab.com/larry1123/cloudflared/badges/master/pipeline.svg)](https://gitlab.com/larry1123/cloudflared/badges/tree/master)

[Cloudflared](https://github.com/cloudflare/cloudflared) cli packaged in an alpine docker image.

Setup to run as non root. Uses `dumb-init` as entrypoint.

Usage
-------------

Tunnel auth
```shell script
docker run --rm -it -v $(pwd)/.cloudflared/:/home/cloudflare/.cloudflared/ registry.gitlab.com/larry1123/cloudflared:latest cloudflared login
docker run --rm -it -v $(pwd)/.cloudflared/:/home/cloudflare/.cloudflared/ registry.gitlab.com/larry1123/cloudflared:latest cloudflared tunnel create name
docker run --rm -it -v $(pwd)/.cloudflared/<UUID>.json:/home/cloudflare/.cloudflared/<UUID>.json registry.gitlab.com/larry1123/cloudflared:latest cloudflared tunnel run --url=hello_world <UUID>
```

```dockerfile
FROM registry.gitlab.com/larry1123/cloudflared
ARG UUID
ENV UUID ${UUID}
COPY .cloudflared/${UUID}.json /home/cloudflare/.cloudflared/
CMD ["cloudflared", "tunnel", "run", "--url=hello_world", "${UUID}"]
```

Classic
```shell script
docker run --rm -it -v $(pwd)/.cloudflared/:/home/cloudflare/.cloudflared/ registry.gitlab.com/larry1123/cloudflared:latest cloudflared login
docker run --rm -it -v $(pwd)/.cloudflared/:/home/cloudflare/.cloudflared/ registry.gitlab.com/larry1123/cloudflared:latest --hostname subdomain.example.com --url=hello_world
```

[Using TryCloudflare](https://developers.cloudflare.com/argo-tunnel/learning/trycloudflare)
```shell script
docker run --rm -it registry.gitlab.com/larry1123/cloudflared:latest cloudflared tunnel --url=hello_world
```

Notes:
* All but the TryCloudflare (it can but will not keep the same domain) examples can be setup to run the tunnels with `--restart unless-stopped`.
* You can use `--network host` to use localhost ie. `--url=http://localhost:8080`
* `--link container` or `--network containerCGroup` is also useful for isolation.

Docker with multi process init example
```dockerfile
FROM YourAppImage:alpine
COPY --from=registry.gitlab.com/larry1123/cloudflared:latest /usr/local/bin/cloudflared /usr/local/bin/cloudflared
### have unit files for both app and cloudfalred
CMD ["/lib/systemd/systemd"]
```

Build Info
-------------
The project's home is on [gitlab](https://gitlab.com/larry1123/cloudflared/).  
Issues and merge request should be made there.  
This project is hosted on [docker hub](https://hub.docker.com/r/larry1123/cloudflared/).
This project's official images are `registry.gitlab.com/larry1123/cloudflared` docker hub is manually updated.

License
-------------
This project is Licensed under the Apache License, Version 2.0.
Artifacts of scripts provided within this repo may have their own licenses.
