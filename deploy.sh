#!/usr/bin/env bash
tag="2023-8-2"

docker pull registry.gitlab.com/larry1123/cloudflared:${tag}
docker tag registry.gitlab.com/larry1123/cloudflared:${tag} larry1123/cloudflared:${tag}
docker push larry1123/cloudflared:${tag}
